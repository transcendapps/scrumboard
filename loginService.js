app.factory('loginService', function($http, $q, $log){
    return {
        attemptLogin: attemptLogin
    }
    
    function attemptLogin(username, password){
        var deferred = $q.defer();
        var params = {
            username: username,
            password: password
        };
        $http.post("http://transcendapps.mynetgear.com:64001/login", {params: params}).then(function(response) {
            deferred.resolve(response);
        }).catch(function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
});