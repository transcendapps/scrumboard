var gulp = require("gulp")                // Gulp itself.
var rimraf = require("gulp-rimraf");
// Define a helper object, to point to various paths in our project.
var paths = {
    webroot: "/var/www/html",
    home: "./"
};

paths.deploySrc = paths.home+"/**";
paths.deployDest = paths.webroot + "/scrumboard";

gulp.task('deploy', function () {
    gulp.src([paths.deploySrc])
        .pipe(gulp.dest(paths.deployDest));
});

gulp.task("clean", function(cb){
    try {
        gulp.src(paths.deployDest+"/**").pipe(rimraf({force:true}));
    }catch(err){
        
    }
})

gulp.task("build",["clean","deploy"]);

gulp.task("default",["build"]);