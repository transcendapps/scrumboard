(function(){
    'use strict';
    app.directive('login', login).controller('LoginController', loginController);
    
    /**
     * @ngInject
     */
    
    function login(){
        return {
            restrict: 'E',
            scope: {
                details : '='
            },
            templateUrl: 'login.html',
            controller: 'LoginController',
            controllerAs: 'login',
            replace: true,
            bindToController: true
        };
    }
    
    
    /**
     * @ngInject
     */
     
    function loginController($scope, $log, loginService){
        var login = this;
        login.submitLogin = submitLogin;
        
        function submitLogin(){
            var authenticated = false;
            //balhablhanlha
            loginService.attemptLogin(login.username, login.password).then(function(respose) {
                authenticated = true;
                $scope.$emit('loggedIn'); 
                toastr.success("Logged In Successfully");
            }).catch(function(err){
                toastr.error("Username/Password combination not found", "Login Failed!");
            });
        }
    }
})();