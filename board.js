(function(){
    'use strict';
    app.directive('board', board).controller('BoardController', boardController);
    
    /**
     * @ngInject
     */
    
    function board(){
        return {
            restrict: 'E',
            scope: {
            },
            templateUrl: 'board.html',
            controller: 'BoardController',
            controllerAs: 'board',
            replace: true,
            bindToController: true
        };
    }
    
    
    /**
     * @ngInject
     */
     
    function boardController($scope,$log, $http, dragularService, scrumBoardService){
        var board = this;
        board.getBoardData = getBoardData;
        board.putData = putData;
        board.getStoryData = getStoryData;
        board.createStory = createStory;
        board.isProjectStories = false;
        board.projects;
        board.usernames;
        board.columns;
        board.storyId;
        board.startingColumns;

        function getBoardData(){
            //get the data from data.json using http and set the entries to the data
            scrumBoardService.getProjectItems().then(function(response){
                board.isStoryView = false; 
                board.columns = response;
                board.storyId = null;
                board.isProjectStories = false;
                board.startingColumns = angular.copy(board.columns);
            });
        }
        
        $scope.$on('getProjectStories', function(event, data){
            getStoryData(data.id);
        });
        
        $scope.$on('getProjects', function(event, data){
            getBoardData();
        });
        
        $scope.$on('getStories', function(event, data){
            getStoryData();
        });
        
        function getStoryData(identifier){
            if(identifier) {
                scrumBoardService.getProjectStories(identifier).then(function(response){
                  board.isStoryView = true; 
                  board.isProjectStories = true;
                  board.storyId = identifier;
                  board.columns = response; 
                  board.startingColumns = angular.copy(board.columns);
                });
            }
            else{
                scrumBoardService.getStories().then(function(response){
                  board.isStoryView = true; 
                  board.isProjectStories = false;
                  board.columns = response; 
                  board.startingColumns = angular.copy(board.columns);
                });
            }
        }
        
        function putData(){
            _.forEach(board.columns, function(column){
                _.remove(column.items, _.isUndefined);
            })
            var difference = angular.copy(board.columns);
            for(var index = 0; index<difference.length;index++)
            {
                var currentColumn = difference[index];
                var startingComparison = board.startingColumns[index];
                _.forEach(startingComparison.items, function(item){
                    _.remove(currentColumn.items, item);
                });
            }
            $log.debug('Board: ', board);
            if(board.isStoryView){
                scrumBoardService.updateStories(difference);
            }
            else {
                scrumBoardService.updateProjectItems(difference);
            }
        }
        
        function activate(){
            board.getStoryData();
            scrumBoardService.getStoryMetadata().then(function(response){
                board.projects = response[0];
                board.usernames = response[1];
                board.projects.selected = board.projects[0];
                board.usernames.push({username: "Unassigned"});
                board.usernames.selected = "Unassigned";
            });
        }
        
        function createStory(){
            if(board.description && board.projects.selected)
            {
                console.log("Project Selected: ", board.projects.selected);
                console.log("Project Selected: ", board.usernames.selected);
                if(board.usernames.selected==='Unassigned')
                    board.usernames.selected = null;
                scrumBoardService.createStory({
                        description: board.description,
                        project_item_code: board.projects.selected,
                        username: board.usernames.selected
                }).then(function(){
                    activate();    
                });
            }
            else
            {
                toastr.error("One or more of the inputs were missing","Couldnt Create Story");
            }
        }
        
        activate();
        
        var defaultDragularOptions = {
          revertOnSpill: true,               // item returns to original place
          classes: {
            mirror: 'custom-green-mirror',
          },
          nameSpace: 'common'
        } // just connecting left and right container
        dragularService('.column', defaultDragularOptions);
        
    }
})();