(function(){
    'use strict';
    app.directive('item', item).controller('ItemController', itemController);
    
    /**
     * @ngInject
     */
    
    function item(){
        return {
            restrict: 'E',
            scope: {
                details : '='
            },
            templateUrl: 'item.html',
            controller: 'ItemController',
            controllerAs: 'item',
            replace: true,
            bindToController: true
        };
    }
    
    
    /**
     * @ngInject
     */
     
    function itemController($scope, $log){
        var item = this;
        item.getData = function(){
            $scope.$emit('getProjectStories', {id: item.details.project_item_code});
        }
        
        function activate() {
            if(item.details.color) {
                item.color = {
                    'border-left-color' : hashColor()
                };
            }
        }
        
        function hashColor() {
            var hash = 0, i, chr, len;
            if (item.details.color.length === 0) return hash;
            for (i = 0, len = item.details.color.length; i < len; i++) {
                chr   = item.details.color.charCodeAt(i);
                hash  = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }
            if(hash < 0)
                hash = hash * -1;
            return ('#' + hash).slice(0,7);
        };
        
        activate();
    }
})();