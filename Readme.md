# How to get the infrastructure of a startup in 15 minutes
## Installs
```
sudo apt-get install xrdp
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add - 
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install mysql-server python-mysqldb
sudo apt-get install phpmyadmin
sudo npm install -g gulp
```

## Config

In `/etc/default/jenkins` change `HTTP_PORT`  
In `/etc/apache2/ports.conf` change `Listen 80` to new port  
In `/etc/apache2/apache2.conf` add `Include /etc/phpmyadmin/apache.conf` to the end of the file  
Restart Jenkins and Apache2  
In Manage Jenkins/Configure System set Jenkins URL to be the external URL  
Install Jenkins BitBucket plugin  
Add Jenkins Credentials with Bitbucket username and password  
`sudo visudo` add `jenkins ALL=(ALL) NOPASSWD:ALL` to the end of the file  
Add user `manager@localhost` to database  
Enable remote access to mysql server  

## Jenkins Job Creation
Set Project name  
Set source control management to git, add BitBucket repository URL, link with BitBucket credentials, set branck specifier to ```**```  
Set Build when a change is pushed to BitBucket to true  
Set Poll SCM to true, set schedule to `*/30****`  
Create Execute Shell Build Step `npm install`  
Create Project Specific Execute Shells  
### For Node Projects:
Create Execute Shell Build Step `(sudo killall node) || (return 0)`  
Create Execute Shell Build Step `BUILD_ID=dontKillMe node /path/to/node/file.js &`  