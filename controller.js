app.controller('ContentController', function($http, $scope, $log){
    var content = this;
    content.isLoggedIn = false;
    
    $scope.$on('loggedIn', function() {
        console.log('LoggedOn');
        content.isLoggedIn = true;
    })
    
});