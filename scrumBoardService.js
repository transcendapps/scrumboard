app.factory('scrumBoardService', function($http, $q, $log){
    return {
        getProjectItems: getProjectItems,
        getStories: getStories,
        getProjectStories: getProjectStories,
        updateProjectItems: updateProjectItems,
        updateStories: updateStories,
        getStoryMetadata: getStoryMetadata,
        createStory: createStory
    };
    
    function getProjectItems(){
        return $http.get("http://transcendapps.mynetgear.com:64001/board").then(function(response){
            return response.data;         
        });
    }
    
    function getProjectStories(projectItemCode){
        if(projectItemCode){
            return $http.get("http://transcendapps.mynetgear.com:64001/board/"+projectItemCode).then(function(response){
                return response.data;         
            });
        }
        return $q.reject();
    }
    
    function getStories(){
        return $http.get("http://transcendapps.mynetgear.com:64001/stories").then(function(response){
            return response.data;         
        });
    }
    
    function updateProjectItems(updatedProjectItems){
        $log.debug('Update', updatedProjectItems);
        return $http.post("http://transcendapps.mynetgear.com:64001/board/update", {update: updatedProjectItems}).then(function(){
            toastr.success("Saved Changes");
        }).catch(function(err){
            toastr.error("Couldnt Save Changes");
        });
    }
    
    function updateStories(updatedProjectStories){
        console.log(updatedProjectStories);
        return $http.post("http://transcendapps.mynetgear.com:64001/stories/update/", 
                          {update: updatedProjectStories}).then(function(){
            toastr.success("Saved Changes");
        }).catch(function(err){
            toastr.error("Couldnt Save Changes");
        });
    }
    
    function getStoryMetadata(){
        return $http.get("http://transcendapps.mynetgear.com:64001/stories/create").then(function(response){
            return response.data;
        });
    }
    
    function createStory(storyData){
        console.log(storyData);
        return $http.post("http://transcendapps.mynetgear.com:64001/stories/create",
                         {story: storyData}).then(function(){
          toastr.success("Created New Story");
        }).catch(function(err){
            toastr.error("Couldnt Create New Story");
        });
    }
});