(function(){
    'use strict';
    app.directive('column', column).controller('ColumnController', columnController);
    
    /**
     * @ngInject
     */
    
    function column(){
        return {
            restrict: 'E',
            scope: {
                items : '=',
                name: '='
            },
            templateUrl: 'column.html',
            controller: 'ColumnController',
            controllerAs: 'column',
            replace: true,
            bindToController: true
        };
    }
    
    
    /**
     * @ngInject
     */
     
    function columnController($scope, $log, $element, dragularService){
        var column = this;
        dragularService($element, {
            scope: $scope
        });
        $scope.$on('dragulardrop', function(e, el, tc,sc){
            e.stopPropagation();
            try{
                var element = angular.element(el).scope().item;
                var targetModel = angular.element(tc).scope().column.items;
                var sourceModel = angular.element(sc).scope().column.items;
                _.remove(targetModel, _.isUndefined);
                $log.debug("SM",sourceModel);
                $log.debug("EL",element);
                $log.debug("TM",targetModel);
                targetModel.push(element);
                _.remove(sourceModel, function(item){
                    return element.details.name === item.details.name;
                });
            }catch(err){}
        });
    }
})();