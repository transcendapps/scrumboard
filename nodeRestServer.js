
var express = require('express');
var _ = require('lodash');
var bodyParser = require('body-parser')
var mysql = require("mysql");
var fs = require("fs");
var app = express();
var statuses = [];
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());

app.get('/board', function (req, res) {
    try{
        var boardData = [];
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio',
          multipleStatements : true
        });

        connection.query('SELECT * FROM Statuses; SELECT * FROM ProjectItems', function(err, rows, fieldsData) {
            try{
                if (err) {
                  connection.end();
                  res.status(404).send();
                }
                statuses = rows[0];
                var projects = rows[1];
                for(var index = 0;index<statuses.length;index++){
                    boardData.push({
                    "name": statuses[index].status_name,
                    "items": []
                    });
                }
                for(var index = 0;index<projects.length;index++){
                    boardData[projects[index].status-1].items.push({
                        "details":{
                            "name": projects[index].item_name,
                            "project_item_code": projects[index].item_code,
                            "color": projects[index].item_code + "color"
                        }
                    });
                }
                connection.end();
                res.end(JSON.stringify(boardData));
            }catch(err){
                console.log(err);
                res.status(404).send();
            }
        });
    }catch(err){
        console.log(err);
        res.status(503).send();
    }
});

app.get('/board/:projectItemCode', function (req, res) {
    try{
        var boardData = [];
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio',
          multipleStatements : true
        });

        connection.query('SELECT * FROM Statuses; SELECT * FROM Stories WHERE project_item_code = '+req.params.projectItemCode,
                         function(err, rows, fieldsData){
            if(err) {
                connection.end();
                res.status(404).send();
            }
            try{
                statuses = rows[0];
                var stories = rows[1];

                for(var index = 0;index<statuses.length;index++){
                    boardData.push({
                        "name": statuses[index].status_name,
                        "items": []
                    });
                }
                for(var index = 0;index<stories.length;index++){
                   boardData[stories[index].story_status-1].items.push({
                      "details":{
                         "name": stories[index].description,
                          "story_code": stories[index].story_code,
                          "color": stories[index].project_item_code + "color"
                      }
                   });
                }
                connection.end();
                res.end(JSON.stringify(boardData));
            }catch(err) {
                connection.end();
                res.status(404).send();
            }

        });
    }catch(err){
        console.log(err);
        res.status(503).send();
    }
});

app.get('/stories', function (req, res) {
    try{
        var boardData = [];
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio',
          multipleStatements : true
        });

        connection.query('SELECT * FROM Statuses; SELECT * FROM Stories',
                         function(err, rows, fieldsData){
            if(err) {
                connection.end();
                res.status(404).send();
            }
            try{
                statuses = rows[0];
                var stories = rows[1];

                for(var index = 0;index<statuses.length;index++){
                    boardData.push({
                        "name": statuses[index].status_name,
                        "items": []
                    });
                }
                for(var index = 0;index<stories.length;index++){
                   boardData[stories[index].story_status-1].items.push({
                      "details":{
                         "name": stories[index].description,
                          "story_code": stories[index].story_code,
                          "color": stories[index].project_item_code + "color"
                      }
                   });
                }
                connection.end();
                res.end(JSON.stringify(boardData));
            }catch(err) {
                connection.end();
                res.status(404).send();
            }

        });
    }catch(err){
        console.log(err);
        res.status(503).send();
    }
});

app.get('/stories/create', function (req, res) {
    try{
        var boardData = [];
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio',
          multipleStatements : true
        });

        connection.query('SELECT item_name, item_code FROM ProjectItems; SELECT username FROM Members',
                         function(err, rows, fieldsData){
            if(err) {
                res.status(404).send();
            }
            try{
                var projects = [];
                var usernames = [];
                connection.end();
                res.end(JSON.stringify(rows));
            }catch(err) {
                connection.end();
                res.status(404).send();
            }
        });
    }catch(err){
        console.log(err);
        res.status(503).send();
    }
});

app.post('/board/update', function (req, res) {
    try{
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio',
          multipleStatements : true
        });
        connection.beginTransaction(function(err) {
          if (err) {     
              connection.end();
              res.status(503).send();
          }
          var board = req.body.update;
          var updates = [];
          _.forEach(board, function(column){
              var status = 0;
              for(var index = 0; index < statuses.length; index++){
                  if(statuses[index].status_name==column.name)
                  {
                      
                      status = statuses[index].code;
                      break;
                  }
              }
             _.forEach(column.items, function(item){
               updates.push({status: status, item_code: item.details.project_item_code});  
             });
          });
          _.forEach(updates, function(update){
            connection.query('UPDATE ProjectItems SET status=? WHERE item_code=?', [update.status, update.item_code], function(err, result) {
              if (err) {
                return connection.rollback(function() {
                  connection.end();
                  res.status(503).send();
                });
              }
            });
          });
          connection.commit(function(err) {
            connection.end();
            if (err) {
              return connection.rollback(function() {
                  res.status(503).send();
              });
            }
            res.status(200).send();
          }); 
        });
    }catch(err){
        res.status(503).send();
    }
});

app.post('/stories/update', function (req, res) {
    try{
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio',
          multipleStatements : true
        });
        connection.beginTransaction(function(err) {
          if (err) {  
            connection.end();
            res.status(503).send();
          }
          var board = req.body.update;
          var updates = [];
          _.forEach(board, function(column){
              var status = 0;
              for(var index = 0; index < statuses.length; index++){
                  if(statuses[index].status_name==column.name)
                  {
                      status = statuses[index].code;
                      break;
                  }
              }
             _.forEach(column.items, function(item){
               updates.push({status: status, story_code: item.details.story_code});  
             });
          });
          _.forEach(updates, function(update){
            connection.query('UPDATE Stories SET story_status=? WHERE story_code=?', [update.status, update.story_code], function(err, result) {
              if (err) {
                return connection.rollback(function() {
                    connection.end();
                    res.status(503).send();
                });
              }
            });
          });
          connection.commit(function(err) {
            connection.end();
            if (err) {
              return connection.rollback(function() {
                    res.status(503).send();
              });
            }
            res.status(200).send();
          });
        });
    }catch(err){
        res.status(503).send();
    }
});

app.post('/stories/create', function (req, res) {
    try{
        var boardData = [];
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio'
        });

        var story = req.body.story;
        connection.query('INSERT INTO Stories (project_item_code, username, description, story_status) VALUES(?,?,?,?)', [story.project_item_code, story.username, story.description, '1'], function(err, rows, fieldsData){
            connection.end();
            if(err){
                res.status(403).send();
            }
            res.status(201).send();
        });
    }catch(err){
        console.log(err);
        res.status(503).send();
    }
});

app.post('/login', function (req, res) {
    try{
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'manager',
          password : 'bookTable43',
          database : 'portfolio'
        });

        var params = req.body.params;
        var password = params.password;
        //do some hashing
        connection.query('SELECT * FROM Members WHERE username = ? AND p_word = ?', [params.username, password], 
                         function(err, rows, fieldsData){
            connection.end();
            if(err || rows.length <= 0){
                res.status(201).send();
                res.status(403).send();
            }
            res.status(201).send();
        });
    }catch(err){
        console.log(err);
        res.status(201).send();
        res.status(403).send();
    }
});

var server = app.listen(64001, function () {
	var host = server.address().address
	var port = server.address().port
});